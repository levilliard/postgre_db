
-- LAE PRO ( logiciel Applique a l'Ecole Pro)
-- DB 1.00
-- Copyriht LAE PRO 2017
-- levilliard@gmail.com, TizonDife


-- SCHEMA: lae_db

-- DROP SCHEMA lae_db ;

CREATE SCHEMA lae_db
    AUTHORIZATION postgres;

COMMENT ON SCHEMA lae_db
    IS 'standard public schema';

GRANT ALL ON SCHEMA lae_db TO postgres;


-- create table address

CREATE TABLE lae_db.address(
    address_id serial PRIMARY KEY,
    address_contry character varying(30) NOT NULL,
    adddress_state character varying(45) NOT NULL,
    address_city character varying(45),
    address_street character varying(100),
    address_zipcode character varying(10),
    address_details character varying(100),
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6) 
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.address OWNER TO postgres;


-- create table institution

CREATE TABLE lae_db.institution(
    institution_id serial PRIMARY KEY,
    address_id integer NOT NULL,
    institution_name character varying(200),
    institution_details character varying(300),
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6),
    CONSTRAINT inst_addr_fk FOREIGN KEY (address_id)
    REFERENCES lae_db.address (address_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE 
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.institution OWNER TO postgres;

-- create table institution_class

CREATE TABLE lae_db.institution_class
(
    institution_class_id serial PRIMARY KEY,
    institution_id integer NOT NULL,
    institution_class_name character varying(45) NOT NULL,
    institution_class_level character varying(20) NOT NULL,
    institution_class_desc character varying(200),
    institution_class_code character varying(10),
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6),
    CONSTRAINT institution_class_fk FOREIGN KEY (institution_id)
    REFERENCES lae_db.institution (institution_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.institution_class
    OWNER to postgres;

-- Table: lae_db.parent

-- DROP TABLE lae_db.parent;

CREATE TABLE lae_db.parent
(
    parent_id serial PRIMARY KEY,
    address_id integer NOT NULL,
    parent_firstname character varying(40) NOT NULL,
    parent_lastname character varying(40) NOT NULL,
    parent_title character varying(50) NOT NULL,
    parent_sex char,
    parent_tels character varying(40),
    parent_email character varying(100),
    parent_religion character varying(100),
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6),
    CONSTRAINT parent_addr_fk FOREIGN KEY (address_id)
    REFERENCES lae_db.address (address_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.parent
    OWNER to postgres;

--create person ref type table

CREATE TABLE lae_db.person_ref
(
    person_ref_id serial PRIMARY KEY,
    address_id integer NOT NULL,
    person_ref_firstname character varying(40) NOT NULL,
    person_ref_lastname character varying(40) NOT NULL,
    person_ref_sex char,
    person_ref_tels character varying(40),
    person_ref_email character varying(100),
    person_ref_desc character varying(100) NOT NULL,
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6),
    CONSTRAINT person_ref_addr_fk FOREIGN KEY (address_id)
    REFERENCES lae_db.address (address_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.person_ref
    OWNER to postgres;

-- Table: lae_db.student

-- DROP TABLE lae_db.student;

CREATE TABLE lae_db.student
(
    student_id serial PRIMARY KEY,
    institution_id integer NOT NULL,
    parent_id integer NOT NULL,
    person_ref_id integer NOT NULL,
    institution_class_id integer NOT NULL,
    address_id integer NOT NULL,
    student_firstname character varying(40) NOT NULL,
    student_lastname character varying(40) NOT NULL,
    student_birthday date NOT NULL,
    student_birthplace character varying(100),
    student_sex char,
    student_mother_tonge character varying(45),
    student_skils character varying(200),
    student_tels character varying(40),
    student_email character varying(100),
    student_addr character varying(100),
    student_religion character varying(20),
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6),

    CONSTRAINT student_inst_fk FOREIGN KEY (institution_id)
        REFERENCES lae_db.institution (institution_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT student_parent_fk FOREIGN KEY (parent_id)
        REFERENCES lae_db.parent (parent_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT student_personf_fk FOREIGN KEY (person_ref_id)
        REFERENCES lae_db.person_ref (person_ref_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT student_class_fk FOREIGN KEY (institution_class_id)
        REFERENCES lae_db.institution_class (institution_class_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT student_add_fk FOREIGN KEY (address_id)
        REFERENCES lae_db.address (address_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.student
    OWNER to postgres;

-- Table: lae_db.teacher

CREATE TABLE lae_db.teacher
(
    teacher_id serial PRIMARY KEY,
    address_id integer NOT NULL,
    teacher_firstname character varying(40) NOT NULL,
    teacher_lastname character varying(40) NOT NULL,
    teacher_level character varying(20) NOT NULL,
    teacher_domain character varying(60) NOT NULL,
    teacher_birthday date NOT NULL,
    teacher_sex char,
    teacher_tels character varying(40),
    teacher_email character varying(100),
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6),

    CONSTRAINT teacher_add_fk FOREIGN KEY (address_id)
        REFERENCES lae_db.address (address_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.teacher
    OWNER to postgres; 

-- create table cours

CREATE TABLE lae_db.cours(
    cours_id serial PRIMARY KEY,
    cours_level character varying(20),
    cours_title character varying(200),
    cours_desc character varying(300),
    created_by character varying(45),
    date_created timestamp(6),
    modified_by character varying(45),
    date_modified timestamp(6)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.cours
    OWNER to postgres; 



-- create table parent_student

CREATE TABLE lae_db.parent_student(
    parent_student_id serial PRIMARY KEY,
    parent_id integer NOT NULL,
    student_id integer NOT NULL,

    CONSTRAINT parent_student_pr_fk FOREIGN KEY (parent_id)
        REFERENCES lae_db.parent (parent_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT parent_student_st_fk FOREIGN KEY (student_id)
        REFERENCES lae_db.student (student_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.parent_student OWNER to postgres;

-- create table teacher_class

CREATE TABLE lae_db.teach_in_class(
    teach_in_class_id serial PRIMARY KEY,
    teacher_id integer NOT NULL,
    institution_class_id integer NOT NULL,

    CONSTRAINT teach_in_th_fk FOREIGN KEY (teacher_id)
        REFERENCES lae_db.teacher (teacher_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT teach_in_ch_fk FOREIGN KEY (institution_class_id)
        REFERENCES lae_db.institution_class (institution_class_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.teach_in_class
    OWNER to postgres;

-- create table teach_cours

CREATE TABLE lae_db.teach_cours(
    teach_cours_id serial PRIMARY KEY,
    teacher_id integer NOT NULL,
    cours_id integer NOT NULL,

    CONSTRAINT teach_crs_th_fk FOREIGN KEY (teacher_id)
        REFERENCES lae_db.teacher (teacher_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT teach_crs_crs_fk FOREIGN KEY (cours_id)
        REFERENCES lae_db.cours (cours_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.teach_cours OWNER to postgres;


-- create table cours inst class

CREATE TABLE lae_db.class_cours(
    class_cours_id serial PRIMARY KEY,
    institution_class_id integer NOT NULL,
    cours_id integer NOT NULL,

    CONSTRAINT teach_crs_inst_fk FOREIGN KEY (institution_class_id)
        REFERENCES lae_db.institution_class (institution_class_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,

    CONSTRAINT teach_crs_crs_cls FOREIGN KEY (cours_id)
        REFERENCES lae_db.cours (cours_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE lae_db.class_cours OWNER to postgres;

-- insert data 

insert into lae_db.address(address_contry, adddress_state, address_city, address_street, address_zipcode, address_details, created_by, date_created, modified_by, date_modified)
    values('HAITI', 'OUEST', 'Croix-des-Bouquets', '34, Impasse Test, Villiard Pacifique', 'HT4510', ' ', 'levilliard', '2017-09-16', '', '2017-09-16');
insert into lae_db.address(address_contry, adddress_state, address_city, address_street, address_zipcode, address_details, created_by, date_created, modified_by, date_modified)
    values('HAITI', 'OUEST', 'Croix-des-Bouquets', '23, Ruelle Arzace, Beudet 12', 'HT4510', '', 'levilliard', '2017-09-16', '', '2017-09-16');
insert into lae_db.address(address_contry, adddress_state, address_city, address_street, address_zipcode, address_details, created_by, date_created, modified_by, date_modified)
    values('HAITI', 'OUEST', 'Croix-des-Bouquets', '50, Ruelle Jenjacques, Lilavois 54', 'HT4510', '', 'levilliard', '2017-09-16', '', '2017-09-16');
insert into lae_db.address(address_contry, adddress_state, address_city, address_street, address_zipcode, address_details, created_by, date_created, modified_by, date_modified)
    values('HAITI', 'OUEST', 'Delmas 43', '50, Ruelle Pierre', 'HT9510', '', 'levilliard', '2017-09-16', '', '2017-09-16');
    
insert into lae_db.institution(address_id, institution_name, institution_details, created_by, date_created, modified_by, date_modified)
    values(1, 'Le Petit Monde', 'Ecole Primaire', 'levilliard', '2017-09-16', '', '2017-09-16');

insert into lae_db.institution_class(institution_id, institution_class_name, institution_class_level, institution_class_desc, institution_class_code, created_by, date_created, modified_by, date_modified)
    values(1, 'Classe Papillon', '1ere Annee', 'Pour les amusants', 'CL001A', 'levilliard', '2017-09-16', '', '2017-09-16');

insert into lae_db.parent(address_id, parent_firstname, parent_lastname, parent_title, parent_sex, parent_tels, parent_email, parent_religion, created_by, date_created, modified_by, date_modified)
    values(2, 'Jean', 'Pierre-Louis', 'Ing. Informatique, Turbo System S.A.', 'M', '50999009900', 'pp@test.com', 'Adventiste', 'levilliard', '2017-09-16', '', '2017-09-16');

insert into lae_db.person_ref(address_id, person_ref_firstname, person_ref_lastname, person_ref_sex, person_ref_tels, person_ref_email, person_ref_desc, created_by, date_created, modified_by, date_modified)
    values(3, 'Sam', 'Joseph', 'M', '50999679900', 'pp@test.com', 'Oncle', 'levilliard', '2017-09-16', '', '2017-09-16');

insert into lae_db.student(institution_id, parent_id, person_ref_id, institution_class_id, address_id, student_firstname, student_lastname, student_birthday, student_birthplace, student_sex, student_mother_tonge, student_skils, student_tels, student_email, student_religion, created_by, date_created, modified_by, date_modified)
    values(1, 1, null, 1, 2, 'Jeff', 'Pierre-Louis', '2012-09-16', 'Croix-des-Bouquets', 'M', 'Creole', 'Musique', '', '', 'Adventiste', 'levilliard', '2017-09-16', '', '2017-09-16');

insert into lae_db.teacher(address_id, teacher_firstname, teacher_lastname, teacher_level, teacher_domain, teacher_birthday, teacher_sex, teacher_tels, teacher_email, created_by, date_created, modified_by, date_modified)
    values(4, 'Taylor', 'Guerrier', 'Secondaire', 'Math', '1980-04-15', 'M', '50945343289', 'tc@test.com', 'levilliard', '2017-09-16', '', '2017-09-16');
