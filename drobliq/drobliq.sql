
-- DrObliQ PRO ()
-- DB CORE 1.00
-- Copyriht ObliqSystem 2017
-- levilliard@gmail.com


-- DB: DROBLIQ_DB
-- SCHEMA: DROBLIQ_SCH_CORE  ;


-- create db drobliq_db

CREATE DATABASE DROBLIQ_DB
  WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = 5;

-- create schema drobliq_sch_core
CREATE SCHEMA DROBLIQ_SCH_CORE
    AUTHORIZATION postgres;

COMMENT ON SCHEMA DROBLIQ_SCH_CORE
    IS 'standard public schema';

GRANT ALL ON SCHEMA DROBLIQ_SCH_CORE TO postgres;


-- create table address

CREATE TABLE DROBLIQ_SCH_CORE.ADDRESS(
    ADDRESS_ID serial PRIMARY KEY,
    ADDRESS_COUNTRY character varying(30) NOT NULL,
    ADDRESS_STATE character varying(45) NOT NULL,
    ADDRESS_CITY character varying(45),
    ADDRESS_STREET character varying(100),
    ADDRESS_ZIPCODE character varying(10),
    ADDRESS_DETAILS character varying(100),
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6) 
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.ADDRESS OWNER TO postgres;



-- create table medical_center

CREATE TABLE DROBLIQ_SCH_CORE.MEDICAL_CENTER(
    MEDICAL_CENTER_ID serial PRIMARY KEY,
    ADDRESS_ID int NOT NULL,
    MEDICAL_CENTER_DETAILS character varying(100),
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6),    
    CONSTRAINT MED_CTR_ADDR_FK FOREIGN KEY (ADDRESS_ID) REFERENCES DROBLIQ_SCH_CORE.ADDRESS (ADDRESS_ID) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.MEDICAL_CENTER OWNER TO postgres;


-- create table patient

CREATE TABLE DROBLIQ_SCH_CORE.PATIENT(
    PATIENT_ID serial PRIMARY KEY,
    ADDRESS_ID integer NOT NULL,
    HOSPITAL_ID integer,
    PATIENT_FIRSTNAME character varying(40) NOT NULL,
    PATIENT_LASTNAME character varying(40) NOT NULL,
    PATIENT_SEX char,
    PATIENT_TELS character varying(40),
    PATIENT_EMAIL character varying(100),
    PATIENT_DATE_OF_BIRTH date,
    PATIENT_WEIGHT real,
    PATIENT_HEIGHT real,
    PATIENT_DETAILS text,
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6), 

    CONSTRAINT PATIENT_ADDR_FK FOREIGN KEY (ADDRESS_ID)
    REFERENCES DROBLIQ_SCH_CORE.ADDRESS (ADDRESS_ID) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.PATIENT
    OWNER to postgres;

-- create table doctor

CREATE TABLE DROBLIQ_SCH_CORE.DOCTOR(
	DOCTOR_ID serial PRIMARY KEY,
    ADDRESS_ID integer NOT NULL,
    DOCTOR_FIRSTNAME character varying(40) NOT NULL,
    DOCTOR_LASTNAME character varying(40) NOT NULL,
    DOCTOR_SEX char,
    DOCTOR_TELS character varying(40),
    DOCTOR_EMAIL character varying(100),
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6), 

    CONSTRAINT DOCTOR_ADDR_FK FOREIGN KEY (ADDRESS_ID) REFERENCES DROBLIQ_SCH_CORE.ADDRESS(ADDRESS_ID)
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH(
	OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.DOCTOR
    OWNER to postgres;

-- create table visits

CREATE TABLE DROBLIQ_SCH_CORE.VISIT(
	VISIT_ID serial PRIMARY KEY,
    PATIENT_ID integer NOT NULL,
    DOCTOR_ID integer NOT NULL,
    VISIT_DETAILS text,
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6), 

    CONSTRAINT VISIT_PATIENT_FK FOREIGN KEY (PATIENT_ID) REFERENCES DROBLIQ_SCH_CORE.PATIENT(PATIENT_ID)
    ON UPDATE NO ACTION
    ON DELETE CASCADE,

    CONSTRAINT VISIT_DOCTOR_FK FOREIGN KEY (DOCTOR_ID) REFERENCES DROBLIQ_SCH_CORE.DOCTOR(DOCTOR_ID)
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH(
	OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.VISIT
    OWNER to postgres;

-- create table medication

CREATE TABLE DROBLIQ_SCH_CORE.MEDICATION(
	MEDICATION_ID serial PRIMARY KEY,
    MEDICATION_NAME character varying(100),
    MEDICATION_COST real,
    MEDICATION_DETAILS text,
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6)
)
WITH(
	OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.MEDICATION
    OWNER to postgres;


-- create table medication

CREATE TABLE DROBLIQ_SCH_CORE.MEDICAL_PROCEDURE(
	MEDICAL_PROCEDURE_ID serial PRIMARY KEY,
    MEDICAL_PROCEDURE_NAME character varying(100),
    MEDICAL_PROCEDURE_COST real,
    MEDICAL_PROCEDURE_DETAILS text,
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6)
)
WITH(
	OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.MEDICAL_PROCEDURE
    OWNER to postgres;


-- There is a many-to-many relation between medication and visit tables 
-- There is a many-to-many relation between medical_procedure and visit tables 
-- visit_medication and visit_procedure will be the intermediate tables


-- create table visit_medication

CREATE TABLE DROBLIQ_SCH_CORE.VISIT_MEDICATION(
    VISIT_ID integer,
    MEDICATION_ID integer,
    VISIT_MEDICATION_QTY integer,
    VISIT_MEDICATION_DERIVED_COST real,
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6),

    CONSTRAINT VISIT_MEDICATION_FK1 FOREIGN KEY (VISIT_ID) REFERENCES DROBLIQ_SCH_CORE.VISIT(VISIT_ID)
    ON UPDATE NO ACTION
    ON DELETE CASCADE,

    CONSTRAINT VISIT_MEDICATION_FK2 FOREIGN KEY (MEDICATION_ID) REFERENCES DROBLIQ_SCH_CORE.MEDICATION(MEDICATION_ID)
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH(
	OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.VISIT_MEDICATION
    OWNER to postgres;



-- create table visit_procedure

CREATE TABLE DROBLIQ_SCH_CORE.VISIT_PROCEDURE(
    VISIT_ID integer,
    MEDICAL_PROCEDURE_ID integer,
    VISIT_PROCEDURE_QTY integer,
    VISIT_PROCEDURE_DERIVED_COST real,
    CREATED_BY character varying(45),
    DATE_CREATTED timestamp(6),
    MODIFIED_BY character varying(45),
    DATE_MODIFIED timestamp(6),

    CONSTRAINT VISIT_PROCEDURE_FK1 FOREIGN KEY (VISIT_ID) REFERENCES DROBLIQ_SCH_CORE.VISIT(VISIT_ID)
    ON UPDATE NO ACTION
    ON DELETE CASCADE,

    CONSTRAINT VISIT_PROCEDURE_FK2 FOREIGN KEY (MEDICAL_PROCEDURE_ID) REFERENCES DROBLIQ_SCH_CORE.MEDICAL_PROCEDURE(MEDICAL_PROCEDURE_ID)
    ON UPDATE NO ACTION
    ON DELETE CASCADE
)
WITH(
	OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE DROBLIQ_SCH_CORE.VISIT_PROCEDURE
    OWNER to postgres;

-- END OF FILE